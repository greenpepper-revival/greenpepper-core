package com.greenpepper.interpreter.collection;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;

import com.greenpepper.document.FakeSpecification;
import com.greenpepper.interpreter.ListOfInterpreter;
import com.greenpepper.interpreter.SetOfInterpreter;
import com.greenpepper.interpreter.SubsetOfInterpreter;
import com.greenpepper.interpreter.SupersetOfInterpreter;
import com.greenpepper.reflect.DefaultFixture;
import com.greenpepper.util.Tables;
import junit.framework.TestCase;

import com.greenpepper.reflect.CollectionProvider;
import com.greenpepper.reflect.Fixture;
import com.greenpepper.reflect.PlainOldFixture;
import org.junit.Test;

import static com.greenpepper.Assertions.*;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;

public class CollectionInterpreterTest {
    CollectionInterpreter collectionInterpreter;
    Collection collection;
    private Tables tables;

    private FakeSpecification document()
    {
        return new FakeSpecification( tables );
    }


    private CollectionInterpreter interpreterFor(Object target)
    {
        return  new CollectionInterpreter(new PlainOldFixture(target)) {};
    }

    @Test
    public void testThatATargetWithNoWayToGetACollectionThrowsAnException() throws Exception
    {
        try {
            collectionInterpreter = interpreterFor(this);
            collectionInterpreter.getFixtureList();
            fail();
        }
        catch (Exception ex)
        {
            // ok;
        }
    }

    @Test
    public void testThatTheQueryMethodIsCalledForAClass() throws Exception
    {
        String firstElement = "TargetWithAQueryMethod-FirstElement";
        TargetWithAQueryMethod target = new TargetWithAQueryMethod(firstElement);

        collectionInterpreter = interpreterFor(target);

        assertEquals(firstElement, firstElement(collectionInterpreter.getFixtureList()));

    }

    @Test
    public void testThatATargetThatIsACollectionIsUsed() throws Exception
    {
        String firstElement = "TargetThatIsACollection-FirstElement";

        TargetThatIsACollection target = new TargetThatIsACollection(firstElement);

        collectionInterpreter = interpreterFor(target);

        assertEquals(firstElement, firstElement(collectionInterpreter.getFixtureList()));
    }

    @Test
    public void testThatACollectionProviderAnnotationIsThePreferedCall() throws Exception
    {
        String firstElement = "TargetWithACollectionProviderAnnotation-FirstElement";
        TargetWithACollectionProviderAnnotation target = new TargetWithACollectionProviderAnnotation(firstElement);

        collectionInterpreter = interpreterFor(target);

        assertEquals(firstElement, firstElement(collectionInterpreter.getFixtureList()));
    }


    @Test
    public void listOfShouldHandleGlobalExpectedColumns()
            throws Exception
    {
        ListOfInterpreter interpreter = new ListOfInterpreter(new DefaultFixture(new CollectionInterpreterTest.FixtureThatHasDefaultValuesCombinason()));

        theListsShouldHandleGlobalExpectedColumns(interpreter);
    }

    @Test
    public void setOfShouldHandleGlobalExpectedColumns() throws Exception {
        SetOfInterpreter interpreter = new SetOfInterpreter(new DefaultFixture(new CollectionInterpreterTest.FixtureThatHasDefaultValuesCombinason()));
        theSetsShouldHandleGlobalExpectedColumns(interpreter, "set of");

        assertEquals("Should have 4 tests (2 right, 1 missing, 1 surplus)", 4, interpreter.statistics().totalCount());
        assertEquals("Should have 2 right", 2, interpreter.statistics().rightCount());
        assertEquals("Should have 2 wrong", 2, interpreter.statistics().wrongCount());

        assertAnnotatedMissing(tables.at(0, 3, 0));
        assertAnnotatedRight(tables.at(0, 4, 0));
        assertAnnotatedSurplus(tables.at(0, 5, 0));
    }

    @Test
    public void subsetOfShouldHandleGlobalExpectedColumns() throws Exception {
        SubsetOfInterpreter interpreter = new SubsetOfInterpreter(new DefaultFixture(new CollectionInterpreterTest.FixtureThatHasDefaultValuesCombinason()));
        theSetsShouldHandleGlobalExpectedColumns(interpreter, "subset of");

        assertEquals("Should have 3 tests (2 right, 1 missing)", 3, interpreter.statistics().totalCount());
        assertEquals("Should have 2 right", 2, interpreter.statistics().rightCount());
        assertEquals("Should have 1 wrong", 1, interpreter.statistics().wrongCount());

        assertAnnotatedMissing(tables.at(0, 3, 0));
        assertAnnotatedRight(tables.at(0, 4, 0));
    }

    @Test
    public void supersetOfShouldHandleGlobalExpectedColumns() throws Exception {
        SupersetOfInterpreter interpreter = new SupersetOfInterpreter(new DefaultFixture(new CollectionInterpreterTest.FixtureThatHasDefaultValuesCombinason()));
        theSetsShouldHandleGlobalExpectedColumns(interpreter, "superset of");

        assertEquals("Should have 3 tests (2 right, 1 surplus)", 3, interpreter.statistics().totalCount());
        assertEquals("Should have 2 right", 2, interpreter.statistics().rightCount());
        assertEquals("Should have 1 wrong", 1, interpreter.statistics().wrongCount());

        assertNotAnnotated(tables.at(0, 3, 0));
        assertAnnotatedRight(tables.at(0, 4, 0));
        assertAnnotatedSurplus(tables.at(0, 5, 0));
    }

    private void theListsShouldHandleGlobalExpectedColumns(CollectionInterpreter interpreter) {
        tables = Tables.parse(
                "[list of][values]\n" +
                "[a][b (null)][c (not null)][d (true)][e (false)]\n" +
                "[1][][][][]\n" +
                "[2][3][][][]\n" +
                "[3][any][null][false][true]"
        );
        interpreter.interpret(document());

        assertEquals("Should have no exception",0, interpreter.statistics().exceptionCount());
        assertEquals("Should have 3 tests (2 right, 1 wrong)", 3, interpreter.statistics().totalCount());
        assertEquals("Should have 2 right", 2, interpreter.statistics().rightCount());
        assertEquals("Should have 1 wrong", 1, interpreter.statistics().wrongCount());

        assertAnnotatedRight(tables.at(0, 2, 0));
        assertAnnotatedRight(tables.at(0, 2, 1));
        assertAnnotatedRight(tables.at(0, 2, 2));
        assertAnnotatedRight(tables.at(0, 2, 3));
        assertAnnotatedRight(tables.at(0, 2, 4));
        assertAnnotatedRight(tables.at(0, 3, 0));
        assertAnnotatedWrong(tables.at(0, 3, 1));
        assertAnnotatedWrong(tables.at(0, 3, 2));
        assertAnnotatedWrong(tables.at(0, 3, 3));
        assertAnnotatedWrong(tables.at(0, 3, 4));
        assertAnnotatedRight(tables.at(0, 4, 0));
        assertAnnotatedRight(tables.at(0, 4, 1));
        assertAnnotatedRight(tables.at(0, 4, 2));
        assertAnnotatedRight(tables.at(0, 4, 3));
        assertAnnotatedRight(tables.at(0, 4, 4));
    }

    private void theSetsShouldHandleGlobalExpectedColumns(CollectionInterpreter interpreter, String keyword) {
        tables = Tables.parse(
                "[" + keyword + "][values]\n" +
                        "[a][b (null)][c (not null)][d (true)][e (false)]\n" +
                        "[1][][][][]\n" +
                        "[2][3][][][]\n" +
                        "[3][any][null][false][true]"
        );
        interpreter.interpret(document());

        assertEquals("Should have no exception",0, interpreter.statistics().exceptionCount());

        assertAnnotatedRight(tables.at(0, 2, 0));
        assertAnnotatedRight(tables.at(0, 2, 1));
        assertAnnotatedRight(tables.at(0, 2, 2));
        assertAnnotatedRight(tables.at(0, 2, 3));
        assertAnnotatedRight(tables.at(0, 2, 4));
    }


    private String firstElement(List<Fixture> list)
    {
        return (String)list.get(0).getTarget();
    }

    public class TargetWithAQueryMethod {
        Collection<Object> collection = new ArrayList<Object>();

        public TargetWithAQueryMethod(String firstElement)
        {
            collection.add(firstElement);
        }

        public Collection query()
        {
            return collection;
        }
    }

    public class TargetThatIsACollection extends ArrayList
    {
        private static final long serialVersionUID = 1L;

        @SuppressWarnings("unchecked")
        TargetThatIsACollection(String firstElement)
        {
            super(Arrays.asList(new String[]{firstElement}));
        }

        public Collection query()
        {
            throw new UnsupportedOperationException();
        }
    }


    public class TargetWithACollectionProviderAnnotation {
        Collection<Object> collection = new ArrayList<Object>();

        public TargetWithACollectionProviderAnnotation(String firstElement)
        {
            collection.add(firstElement);
        }

        public Collection query()
        {
            throw new UnsupportedOperationException();
        }

        @CollectionProvider
        public Collection otherMethod()
        {
            return collection;
        }

    }

    public static class FixtureThatHasDefaultValuesCombinason {

        public static class Data {
            public Integer a;
            public String b;
            public String c;
            public Boolean d;
            public Boolean e;

            public Data(Integer a, String b, String c, Boolean d, Boolean e) {
                this.a = a;
                this.b = b;
                this.c = c;
                this.d = d;
                this.e = e;
            }
        }

        public Collection<Data> query() {
            ArrayList<Data> list = new ArrayList<Data>();
            list.add(new Data(1,null,"any", Boolean.TRUE, Boolean.FALSE));
            list.add(new Data(2,"any", null, null, null));
            list.add(new Data(3,"any", null, Boolean.FALSE, Boolean.TRUE));
            return list;
        }
    }

}
