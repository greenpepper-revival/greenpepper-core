/*
 * Copyright (c) 2006 Pyxis Technologies inc.
 * This is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * This software is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301 USA,
 * or see the FSF site: http://www.fsf.org.
 */

package com.greenpepper.html;

import com.greenpepper.Example;
import com.greenpepper.annotation.Colors;
import com.greenpepper.annotation.Styles;
import com.greenpepper.document.Document;
import com.greenpepper.util.IOUtil;
import org.junit.Before;
import org.junit.Test;

import java.io.*;
import java.nio.charset.Charset;

import static org.apache.commons.io.IOUtils.toInputStream;
import static org.hamcrest.Matchers.containsString;
import static org.hamcrest.Matchers.lessThan;
import static org.junit.Assert.*;

public class HtmlDocumentBuilderTest {

    private HtmlDocumentBuilder builder;
    private String html;
    private HtmlExample example;

    @Before
    public void setUp() throws Exception {
        builder = new HtmlDocumentBuilder("td");
    }

    @Test
    public void testTagMustMatch() {
        html = "<div>html fragment</div>";
        assertNull(parse(html));
    }

    @Test
    public void testTagMustMatchWholeWord() {
        html = "< notatd >cell content</ notatd >";
        assertNull(parse(html));
    }

    private HtmlExample parse(String html) {
        return (HtmlExample) builder.parse(html);
    }

    @Test
    public void testContentShouldBeBodyOfHtmlTag() {
        html = "<td>cell content</td>";
        example = parse(html);
        assertEquals("cell content", example.getContent());
    }

    @Test
    public void testAttributesInTagAreAllowed() {
        html = "< td bgcolor='green'>cell content</ td >";
        example = parse(html);
        assertEquals("cell content", example.getContent());
    }

    @Test
    public void testTagCanBeNestedInOtherElement() {
        html = "<tr><td>a</td><tr>";
        example = parse(html);
        assertEquals("a", example.getContent());
    }

    @Test
    public void testBreadthTraversalIteratesThroughTags() {
        html = "<td>a</td> <td>b</td> <td>c</td>";
        example = parse(html);
        assertEquals("c", example.nextSibling().nextSibling().getContent());
    }

    @Test
    public void testTraversalEndsWithLastTag() {
        html = "<td>a</td>";
        example = parse(html);
        assertNull(example.nextSibling());
    }

    @Test
    public void testTraversalIgnoresTrailingCharacters() {
        html = "<td> </td> more follows ...";
        example = parse(html);
        assertNull(example.nextSibling());
    }

    @Test
    public void testDepthTraversalLooksForNestedTags() {
        html = "<table><tr><td>a</td></tr></table>";
        builder.useTags("table", "tr", "td");
        example = parse(html);
        assertEquals("a", example.firstChild().firstChild().getContent());
    }

    @Test
    public void testSiblingsCanBeAccessedDirectly() {
        html = "<td>a</td> <td>b</td> <td>c</td>";
        example = parse(html);
        assertEquals("c", example.at(2).getContent());
    }

    @Test
    public void testOutOfRangeAccessOfSiblingsGivesNull() {
        html = "<td>a</td>";
        example = parse(html);
        assertNull(example.at(2));
    }

    @Test
    public void testKnowsHowManySiblingsAreLeft() {
        html = "<td>a</td> <td>b</td> <td>c</td>";
        example = parse(html);
        assertEquals(3, example.remainings());
    }

    @Test
    public void testChildrenCanBeAccessedDirectly() {
        html = "<table><tr><td>a</td></tr></table>";
        builder.useTags("table", "tr", "td");
        example = parse(html);
        assertEquals("a", example.at(0, 0, 0).getContent());
    }

    @Test
    public void testOutOfRangeAccessOfChildrenGivesNull() {
        html = "<table><tr><td>a</td></tr></table>";
        builder.useTags("table", "tr", "td");
        example = parse(html);
        assertNull(example.at(1, 2));
        assertNull(example.at(0, 0, 0, 0));
    }

    @Test
    public void testOutputIncludesLeadAndTail() {
        html = "...lead <td> a </td> tail...";
        example = parse(html);
        assertEquals("...lead <td> a </td> tail...", example.toString());
    }

    @Test
    public void testOutputShouldHandleChildrenProperly() {
        html = "<table> ... <tr> ... <td>a</td> ... </tr> ... </table>";
        builder.useTags("table", "tr", "td");

        example = parse(html);
        assertEquals("<table> ... <tr> ... <td>a</td> ... </tr> ... </table>", example.toString());
    }

    @Test
    public void testOutputShouldHandleSiblingsProperly() {
        html = "<tr><td>a</td><td>b</td></tr>";
        builder.useTags("tr", "td");
        example = parse(html);
        assertEquals("<tr><td>a</td><td>b</td></tr>", example.toString());
    }

    @Test
    public void testBackgroundColorCanBeChanged() {
        html = "<td>a</td>";
        example = parse(html);
        example.setStyle("background-color", Colors.GREEN);
        assertEquals("<td style=\"background-color: #AAFFAA;\">a</td>", example.toString());
    }

    @Test
    public void testContentCanBeReplaced() {
        html = "<td>old content</td>";
        example = parse(html);
        example.setContent("new content");
        assertEquals("<td>new content</td>", example.toString());
    }

    @Test
    public void testShouldPreserveExistingAttributes() {
        html = "<td colspan='2'></td>";
        example = parse(html);
        example.setStyle(Styles.BACKGROUND_COLOR, Colors.GRAY);
        example.setContent("a");
        assertEquals("<td colspan='2' style=\"background-color: #CCCCCC;\">a</td>", example.toString());
    }

    @Test
    public void testShouldOverrideExistingStyles() {
        html = "<td style=\"background-color: #CCCCCC; top: 20px;\" ></td>";
        example = parse(html);
        example.setStyle(Styles.BACKGROUND_COLOR, Colors.GREEN);
        example.setContent("a");
        assertEquals("<td style=\"background-color: #CCCCCC; top: 20px; background-color: #AAFFAA !important;\">a</td>", example.toString());
    }

    @Test
    public void testCondensesWhitespaceInText() {
        html = "<td>-\240\240   &nbsp;&nbsp;-</td>";
        example = parse(html);
        assertEquals("- -", example.getContent());
    }

    @Test
    public void testRemovesLeadingAndTrailingWhitespace() {
        html = "<td>  --  </td>";
        example = parse(html);
        assertEquals("--", example.getContent());
    }

    @Test
    public void testConvertsSpecificHtmlEntitiesIntoCharacters() {
        html = "<td>&amp;&lt;&nbsp;&gt;&quot;&apos;&#45;&#42;</td>";
        example = parse(html);
        assertEquals("&< >\"'-*", example.getContent());
    }

    @Test
    public void testConvertsLineBreaksAndParagraphsToLineFeeds() {
        html = "<td>..<br>..<br/>..<br />..<  br  /  >..<br class='with-style'>..</td>";
        example = parse(html);
        assertEquals("..\n..\n..\n..\n..\n..", example.getContent());
    }

    @Test
    public void testOtherHtmlMarkupIsRemoved() {
        html = "<td> <span class='result'> resulting <span/> <b> cell </b> content </td>";
        example = parse(html);
        assertEquals("resulting cell content", example.getContent());
    }

    @Test
    public void testHtmlCommentsAreRemoved() {
        html = "<!--<td>this cell should be removed</td>--><td>this cell has been kept</td>";
        example = parse(html);
        assertEquals("this cell has been kept", example.getContent());
    }

    @Test
    public void testMultipleTagsPerLevelShouldBeSupported() {
        html = "<ul><li>item</li></ul>";
        builder.useTags("table ul", "tr li");
        example = parse(html);
        assertEquals("item", example.at(0, 0).getContent());
    }

    @Test
    public void testTagsOfSameLevelCanBeMixedInHtml() {
        html = "<table><tr><td>cell</td></tr></table>" + "<ul><li>item</li></ul>";
        builder.useTags("table ul", "tr li", "td");
        example = parse(html);
        assertEquals("cell", example.at(0, 0, 0).getContent());
        assertEquals("item", example.at(1, 0).getContent());
    }

    @Test
    public void testStartAndEndTagShouldMatch() {
        html = "not <tr>a valid tag</li>";
        example = (HtmlExample) builder.useTags("tr li th").parse(html);
        assertNull(example);
    }

    @Test
    public void testValidConstructsCanBeEnforcedUsingDescendantSelectors() {
        html = "<table><tr>valid</tr><li>invalid</li></table>";
        builder.useTags("table ol", "table>tr ol>li", "td");
        example = parse(html);
        assertTrue(example.hasChild());
        assertFalse(example.firstChild().hasSibling());
    }

    @Test
    public void testYoungestSiblingShouldBeLastOfFamily() {
        html = "<td>1</td><td>2</td>";
        example = parse(html);
        example.addSibling();
        assertEquals("<td>1</td><td>2</td><td></td>", example.toString());
    }

    @Test
    public void testYoungestChildShouldBeLastOfChildren() {
        html = "<tr><td>1</td></tr>";
        builder.useTags("tr", "td");
        example = parse(html);
        example.addChild();
        assertEquals("<tr><td>1</td><td></td></tr>", example.toString());
    }

    @Test
    public void testFirstBornChildUsesFirstAvailableChildTag() {
        html = "<table></table>";
        builder.useTags("table", "tr th", "td");
        example = parse(html);
        example.addChild().addChild();
        assertEquals("<table><tr><td></td></tr></table>", example.toString());
    }

    @Test
    public void testFiltersCanBeRegisteredToModifyElementContent() {
        builder.addFilter(new BulletListFilter()).useTags("li", "span");
        example = parse("<li>content</li>");
        assertTrue(example.hasChild());
        assertThat(example.firstChild().getContent(), containsString("content"));
    }

    @Test
    public void testTagsCanHaveAnyCaseEvenWhenUsingDescendantSelectors() {
        html = "<Table><TR><Td>cell content</tD></tr></TABLE>";
        example = (HtmlExample) HtmlDocumentBuilder.tablesAndLists().parse(html);
        assertNotNull(example.at(0, 0, 0));
        assertEquals("cell content", example.at(0, 0, 0).getContent());
    }

    @Test
    public void testThatParserDoesNotHangOnBigDocumentWithoutTables() {
        html = junk();
        long start = System.currentTimeMillis();
        example = (HtmlExample) HtmlDocumentBuilder.tablesAndLists().parse(html);
        long end = System.currentTimeMillis();

        assertThat(end - start, lessThan(500L));
    }

    @Test
    public void testThatNameAndExternalLinkAreNullWhenNoMetaTag() throws Exception {
        InputStream reader = toInputStream(" ");
        Document document = HtmlDocumentBuilder.tablesAndLists().build(reader);
        assertNull(document.getName());
        assertNull(document.getExternalLink());
    }

    @Test
    public void testThatWeCanRetrieveTheNameUsingTheMetaTag() throws Exception {
        InputStream reader = toInputStream("<head><meta name=\"title\" content=\"a title\"/>");
        Document document = HtmlDocumentBuilder.tablesAndLists().build(reader);
        assertNotNull(document.getName());
        assertEquals("a title", document.getName());
    }

    @Test
    public void testThatWeCanRetrieveTheExternalLinkUsingTheMetaTag() throws Exception {
        InputStream reader = toInputStream("<head><meta name=\"external-link\" content=\"http://myserver/confluence/space/Spec\"/>");
        Document document = HtmlDocumentBuilder.tablesAndLists().build(reader);
        assertNotNull(document.getExternalLink());
        assertEquals("http://myserver/confluence/space/Spec", document.getExternalLink());
    }

    @Test
    public void testThatWeCanInjectTheCssInBody() throws Exception {
        InputStream reader = toInputStream("<body ng-controller><h1 name=\"title\" content=\"a title\"/></body>");
        Document document = HtmlDocumentBuilder.tablesAndLists().build(reader);
        ByteArrayOutputStream stream = new ByteArrayOutputStream();
        OutputStreamWriter writer = new OutputStreamWriter(stream, Charset.defaultCharset());
        document.print(new PrintWriter(writer, true));
        writer.close();

        InputStream gpReportCssStream = getClass().getResourceAsStream("greenpepper-reports.css");
        InputStreamReader inputStreamReader = new InputStreamReader(gpReportCssStream);
        String css = IOUtil.readContent(inputStreamReader);

        InputStream gpReportJsStream = getClass().getResourceAsStream("greenpepper-reports.js");
        inputStreamReader = new InputStreamReader(gpReportJsStream);
        String js = IOUtil.readContent(inputStreamReader);

        assertEquals("<body ng-controller>" +
                "<script>" + js + "</script>" +
                "<style type=\"text/css\">" + css + "</style>" +
                "<h1 name=\"title\" content=\"a title\"/></body>", stream.toString());

    }

    @Test
    public void testThatWeCanInjectTheCssInBodyprecededwithTag() throws Exception {
        InputStream reader = toInputStream("<html><body><h1 name=\"title\" content=\"a title\"/></body>");
        Document document = HtmlDocumentBuilder.tablesAndLists().build(reader);
        ByteArrayOutputStream stream = new ByteArrayOutputStream();
        OutputStreamWriter writer = new OutputStreamWriter(stream, Charset.defaultCharset());
        document.print(new PrintWriter(writer, true));
        writer.close();

        InputStream gpReportCssStream = getClass().getResourceAsStream("greenpepper-reports.css");
        InputStreamReader inputStreamReader = new InputStreamReader(gpReportCssStream);
        String css = IOUtil.readContent(inputStreamReader);

        InputStream gpReportJsStream = getClass().getResourceAsStream("greenpepper-reports.js");
        inputStreamReader = new InputStreamReader(gpReportJsStream);
        String js = IOUtil.readContent(inputStreamReader);

        assertEquals("<html><body>" +
                "<script>" + js + "</script>" +
                "<style type=\"text/css\">" + css + "</style><h1 name=\"title\" content=\"a title\"/></body>", stream.toString());

    }

    @Test
    public void testListShouldSupportMarkTag() {
        html = "<ul>" +
                "<li>test with <mark>mark</mark> tag</li>" +
                "</ul>";
        HtmlDocumentBuilder builder = HtmlDocumentBuilder.tablesAndLists();
        Example example = builder.parse(html);
        assertEquals("<ul><li><span>test with </span><mark>mark</mark><span> tag</span></li></ul>", example.toString());
        assertEquals(3, example.at(0,0,0).remainings());
    }

    @Test
    public void testListShouldSupportPreTag() {
        html = "<ul>" +
                "<li>test with <pre>mark</pre> tag</li>" +
                "</ul>";
        HtmlDocumentBuilder builder = HtmlDocumentBuilder.tablesAndLists();
        Example example = builder.parse(html);
        assertEquals("<ul><li><span>test with </span><pre>mark</pre><span> tag</span></li></ul>", example.toString());
        assertEquals(3, example.at(0,0,0).remainings());
    }

    @Test
    public void testListShouldSupportStrongTag() {
        html = "<ul>" +
                "<li>test with <Strong>mark</strOng> tag</li>" +
                "</ul>";
        HtmlDocumentBuilder builder = HtmlDocumentBuilder.tablesAndLists();
        Example example = builder.parse(html);
        assertEquals("<ul><li><span>test with </span><Strong>mark</strOng><span> tag</span></li></ul>", example.toString());
        assertEquals(3, example.at(0,0,0).remainings());
    }

    @Test
    public void testListShouldNotSupportDummyTag() {
        html = "<ul>" +
                "<li>test with <dummy>mark</dummy> tag</li>" +
                "</ul>";
        HtmlDocumentBuilder builder = HtmlDocumentBuilder.tablesAndLists();
        Example example = builder.parse(html);
        assertEquals("<ul><li><span>test with <dummy>mark</dummy> tag</span></li></ul>", example.toString());
        assertEquals(1, example.at(0,0,0).remainings());
    }

    @Test
    public void shouldHandleUnneededPTagInLiTag() {
        html = "<ul>" +
                "<li> \t \n <p>test with <strong>strong</strong> tag\n</p> \n </li>" +
                "</ul>";
        HtmlDocumentBuilder builder = HtmlDocumentBuilder.tablesAndLists();
        Example example = builder.parse(html);
        assertEquals("<ul><li><span>test with </span><strong>strong</strong><span> tag\n</span></li></ul>", example.toString());
        assertEquals(3, example.at(0,0,0).remainings());
    }

    @Test
    public void shouldNotBreakDownDoublePTagInLiTag() {
        html = "<ul>" +
                "<li><p>test with <strong>strong</strong> tag</p>" +
                "<p>second paragraphe in li</p>" +
                "</li>" +
                "</ul>";
        HtmlDocumentBuilder builder = HtmlDocumentBuilder.tablesAndLists();
        Example example = builder.parse(html);
        assertEquals(html, example.toString());
        assertEquals(1, example.at(0,0,0).remainings());
    }


    private String junk() {
        StringBuilder buf = new StringBuilder();

        for (int i = 0; i < 1000; i++) {
            buf.append("Lorem Lipsum");
        }

        return buf.toString();
    }

}
