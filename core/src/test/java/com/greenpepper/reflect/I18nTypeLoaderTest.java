package com.greenpepper.reflect;

import com.greenpepper.GreenPepper;
import org.junit.Test;

import java.util.Locale;

import static org.junit.Assert.*;

public class I18nTypeLoaderTest {

    @Test
    public void shouldTranslateClassNameWhenUsingDummy() {
        I18nTypeLoader<?> loader = new I18nTypeLoader<Object>(new CamelCaseTypeLoader<Object>(new JavaTypeLoader<Object>( Object.class) ));
        GreenPepper.setLocale(new Locale("dummy"));
        Type<?> dowith = loader.loadType("Fake it for dowith");
        assertNotNull(dowith);
        assertEquals("DoWith", dowith.getUnderlyingClass().getName());
    }

    @Test
    public void shouldTranslateClassNameWhenUsingFR() {
        I18nTypeLoader<?> loader = new I18nTypeLoader<Object>(new CamelCaseTypeLoader<Object>(new JavaTypeLoader<Object>( Object.class) ));
        GreenPepper.setLocale(new Locale("fr"));
        Type<?> dowith = loader.loadType("Utiliser");
        assertNotNull(dowith);
        assertEquals("DoWith", dowith.getUnderlyingClass().getName());
    }

    @Test
    public void shouldTranslateClassNameWhenUsingNotFound() {
        I18nTypeLoader<?> loader = new I18nTypeLoader<Object>(new CamelCaseTypeLoader<Object>(new JavaTypeLoader<Object>( Object.class) ));
        GreenPepper.setLocale(new Locale("dummy"));
        Type<?> type = loader.loadType("Not Found");
        assertNull(type);
    }

    @Test
    public void shouldGetTheDefaultDoWith() {
        I18nTypeLoader<?> loader = new I18nTypeLoader<Object>(new CamelCaseTypeLoader<Object>(new JavaTypeLoader<Object>( Object.class) ));
        GreenPepper.setLocale(new Locale("dummy"));
        Type<?> type = loader.loadType("do with");
        assertNotNull(type);
        assertEquals("DoWith", type.getUnderlyingClass().getName());
    }
}