package com.greenpepper.dialect;

import org.junit.Test;

import static org.apache.commons.io.IOUtils.toInputStream;
import static org.hamcrest.Matchers.containsString;
import static org.junit.Assert.assertThat;

public class BracketSyntaxDialectTest {

    @Test
    public void testSyntax() throws SpecificationDialectException {
        BracketSyntaxDialect dialect = new BracketSyntaxDialect();

        String text = "[rule for][TestFixture]\n[a][b][sum]\n[1][2][3]";
        String convert = dialect.convert(toInputStream(text));
        assertThat(convert, containsString("<td>rule for</td>"));
    }
}
