package com.greenpepper;

import com.greenpepper.reflect.Type;
import org.junit.Test;

import java.util.Locale;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

public class TypeLoaderChainTest {

    @Test
    public void shouldTranslateClassNameWhenUsingFR() {
        TypeLoaderChain<?> loader = new TypeLoaderChain<Interpreter>(Interpreter.class);
        loader.searchPackage("com.greenpepper.interpreter");
        loader.addSuffix("Interpreter");
        GreenPepper.setLocale(new Locale("fr"));
        Type<?> dowith = loader.loadType("Utiliser");
        assertNotNull(dowith);
        assertEquals("com.greenpepper.interpreter.DoWithInterpreter", dowith.getUnderlyingClass().getName());
    }
}