package com.greenpepper.reflect;

import com.greenpepper.GreenPepper;

import java.util.Enumeration;
import java.util.HashMap;
import java.util.Locale;
import java.util.ResourceBundle;

import static org.apache.commons.lang3.StringUtils.isNotBlank;
import static org.apache.commons.lang3.StringUtils.uncapitalize;

/**
 * Type loader that translate the type name.
 * @param <T>
 */
public class I18nTypeLoader<T> implements TypeLoader<T> {

    private HashMap<String, String> langToDefault = new HashMap<String, String>();
    private final TypeLoader<T> parent;

    public I18nTypeLoader(TypeLoader<T> parent) {
        this.parent = parent;
    }

    @Override
    public void searchPackage(String name) {
        parent.searchPackage(name);
    }

    @Override
    public void addSuffix(String suffix) {
        parent.addSuffix(suffix);
    }

    @Override
    public Type<T> loadType(String name) {
        Type<T> type = parent.loadType(name);
        if (type == null) {
            initLangMap();
            String defaultName = langToDefault.get(uncapitalize(name));
            if (isNotBlank(defaultName)) {
                type = parent.loadType(defaultName);
            }
        }
        return type;
    }

    private void initLangMap() {
        if (langToDefault.isEmpty()) {
            ResourceBundle resourceBundle = GreenPepper.getResourceBundle(GreenPepper.getLocale());
            ResourceBundle defaultResourceBundle = GreenPepper.getResourceBundle(new Locale(""));
            Enumeration<String> keys = resourceBundle.getKeys();
            while(keys.hasMoreElements()) {
                String key = keys.nextElement();
                if (key.startsWith("action.")) {
                    langToDefault.put(uncapitalize(resourceBundle.getString(key)), uncapitalize(defaultResourceBundle.getString(key)));
                }
            }
        }
    }
}
