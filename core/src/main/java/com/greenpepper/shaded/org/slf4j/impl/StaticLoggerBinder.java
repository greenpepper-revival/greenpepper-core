package com.greenpepper.shaded.org.slf4j.impl;

import java.lang.reflect.Method;
import java.net.URL;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.ILoggerFactory;
import org.slf4j.spi.LoggerFactoryBinder;

import com.greenpepper.GreenPepper;

import ch.qos.logback.classic.util.ContextInitializer;
import ch.qos.logback.core.util.Loader;

public class StaticLoggerBinder implements LoggerFactoryBinder {

    private static final String GREENPEPPER_DEBUG_SYSPROP = "greenpepper.debug";

    private static final String LOGGER_BINDER_CLASSNAME = "com.greenpepper.internal.org.slf4j.impl.StaticLoggerBinder";
    private static final String DEFAULT_CONFIG_FILE = "greenpepper-logback-default.xml";
    private static final String TEST_AUTOCONFIG_FILE = "greenpepper-logback-test.xml";
    private static final String AUTOCONFIG_FILE = "greenpepper-logback.xml";
    private static LoggerFactoryBinder internalLoggerBinder;

    private static StaticLoggerBinder SINGLETON = new StaticLoggerBinder();

    static {
        SINGLETON.init();
    }

    private StaticLoggerBinder() {}

    public static StaticLoggerBinder getSingleton() {
        return SINGLETON;
    }

    private void init() {

        String originalLogbackConfigFileValue = System.getProperty(ContextInitializer.CONFIG_FILE_PROPERTY);
        System.setProperty(ContextInitializer.CONFIG_FILE_PROPERTY, findURLOfDefaultConfigurationFile());
        System.setProperty(GREENPEPPER_DEBUG_SYSPROP, Boolean.toString(GreenPepper.isDebugEnabled()));

        // Logback initialisation
        try {
            Class<?> loggerBinderClass = Class.forName(LOGGER_BINDER_CLASSNAME);
            Method getSingletonMethod = loggerBinderClass.getMethod("getSingleton");
            internalLoggerBinder = (LoggerFactoryBinder) getSingletonMethod.invoke(null);
        } catch (Exception e) {
            System.err.print("Unable to instanciate the LoggerFactoryBinder: " + e.getMessage());
            throw new IllegalStateException("Wrong configuration of greenpepper logger.", e);
        }

        if (StringUtils.isNotBlank(originalLogbackConfigFileValue)) {
            System.setProperty(ContextInitializer.CONFIG_FILE_PROPERTY, originalLogbackConfigFileValue);
        } else {
            System.clearProperty(ContextInitializer.CONFIG_FILE_PROPERTY);
        }
        System.clearProperty(GREENPEPPER_DEBUG_SYSPROP);
    }

    void reset() {
        SINGLETON = new StaticLoggerBinder();
        SINGLETON.init();
    }

    private String findURLOfDefaultConfigurationFile() {
        ClassLoader myClassLoader = Loader.getClassLoaderOfObject(this);

        URL url = getResource(TEST_AUTOCONFIG_FILE, myClassLoader);
        if (url != null) {
            return TEST_AUTOCONFIG_FILE;
        }

        url = getResource(AUTOCONFIG_FILE, myClassLoader);

        if (url != null) {
            return AUTOCONFIG_FILE;
        }

        return DEFAULT_CONFIG_FILE;
    }

    private URL getResource(String filename, ClassLoader myClassLoader) {
        return Loader.getResource(filename, myClassLoader);
    }

    @Override
    public ILoggerFactory getLoggerFactory() {
        return internalLoggerBinder.getLoggerFactory();
    }

    @Override
    public String getLoggerFactoryClassStr() {
        return internalLoggerBinder.getLoggerFactoryClassStr();
    }
}
