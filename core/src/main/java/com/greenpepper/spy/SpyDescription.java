package com.greenpepper.spy;

public interface SpyDescription {

    /**
     * The name of the Fixture following the java convention. eg.
     * <pre>| do with | card vendor |</pre> will give <code>CardVendorFixture</code>
     *
     * @return the Camel Case name of the fixture.
     */
    String getName();

    /**
     * The name found in the specification. eg.
     * <pre>| do with | card vendor |</pre> will give <code>card vendor</code>
     *
     * @return the name that were found in the specification.
     */
    String getRawName();
}
