package com.greenpepper.spy;

/**
 * Describes a property of a JAVA Object.
 */
public interface PropertyDescription extends SpyDescription {
}
