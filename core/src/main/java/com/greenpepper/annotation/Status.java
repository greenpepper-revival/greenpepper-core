package com.greenpepper.annotation;

/**
 * <p>Classes class.</p>
 *
 * @author jgi
 * @version $Id: $Id
 */
public enum Status {
    /** Constant <code>SUCCESS = "gp-item-success"</code> */
    SUCCESS ("success"),
    /** Constant <code>FAILLURE = "gp-item-failure"</code> */
    FAILLURE("failure"),
    /** Constant <code>ERROR = "gp-item-error"</code> */
    ERROR("error"),
    /** Constant <code>IGNORED = "gp-item-ignored"</code> */
    IGNORED("ignored"),
    /** Constant <code>SKIPPED = "gp-item-skipped"</code> */
    SKIPPED("skipped");

    private final String gpStatusTag;

    Status(String gpStatusTag) {
        this.gpStatusTag = gpStatusTag;
    }

    public String getGpStatusTag() {
        return gpStatusTag;
    }
}
