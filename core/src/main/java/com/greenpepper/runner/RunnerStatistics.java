/*
 * Copyright (c) 2006 Pyxis Technologies inc.
 * This is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * This software is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301 USA,
 * or see the FSF site: http://www.fsf.org.
 */
package com.greenpepper.runner;

import java.io.Serializable;

import com.greenpepper.Statistics;

/**
 * <p>
 * Runner statistics with per specification granularity.
 * </p>
 * @author Arnaud Daroussin
 * @version $Id: $Id
 */
public final class RunnerStatistics implements Serializable {

    private static final long serialVersionUID = -1L;

    private int rightCount;
    private int wrongCount;
    private int skippedCount;
    private int totalCount;

    /**
     * <p>
     * Constructor for RunnerStatistics.
     * </p>
     */
    public RunnerStatistics() {
        this(0, 0, 0, 0);
    }

    /**
     * <p>
     * Constructor for RunnerStatistics.
     * </p>
     * @param right
     *            set right count.
     * @param wrong
     *            set wrong count.
     * @param skipped
     *            set skipped count.
     * @param total
     *            set total count.
     */
    public RunnerStatistics(int right, int wrong, int skipped, int total) {
        this.rightCount = right;
        this.wrongCount = wrong;
        this.skippedCount = skipped;
        this.totalCount = total;
    }

    /**
     * @return right count.
     */
    public int rightCount() {
        return rightCount;
    }

    /**
     * @return wrong count.
     */
    public int wrongCount() {
        return wrongCount;
    }

    /**
     * @return skipped count.
     */
    public int skippedCount() {
        return skippedCount;
    }

    /**
     * @return total count.
     */
    public int totalCount() {
        return totalCount;
    }

    /**
     * @return notRun count.
     */
    public int notRunCount() {
        return totalCount - rightCount() - wrongCount() - skippedCount();
    }

    /**
     * <p>
     * tally.
     * </p>
     * @param other
     *            a {@link RunnerStatistics} object.
     */
    public void tally(RunnerStatistics other) {
        rightCount += other.rightCount();
        wrongCount += other.wrongCount();
        skippedCount += other.skippedCount();
        totalCount += other.totalCount();
    }

    public void tally(Statistics specStats) {
        if (specStats.hasFailed()) {
            wrong();
        } else if (specStats.rightCount() > 0) {
            right();
        } else {
            skipped();
        }
    }

    public String toString() {
        return String.format("Result for %d specifications: %d right, %d wrong, %d skipped, %d not run", totalCount(), rightCount(), wrongCount(), skippedCount(), notRunCount());
    }

    /**
     * <p>
     * increment right count.
     * </p>
     */
    public void right() {
        rightCount++;
    }

    /**
     * <p>
     * increment wrong count.
     * </p>
     */
    public void wrong() {
        wrongCount++;
    }

    /**
     * <p>
     * increment skipped count.
     * </p>
     */
    public void skipped() {
        skippedCount++;
    }

    /**
     * <p>
     * add to total count.
     * </p>
     */
    public void addToTotal(int total) {
        totalCount += total;
    }

    /**
     * <p>
     * hasFailure.
     * </p>
     * @return a boolean.
     */
    public boolean hasFailure() {
        return wrongCount() > 0;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o)
            return true;
        if (!(o instanceof RunnerStatistics))
            return false;

        RunnerStatistics that = (RunnerStatistics) o;

        return skippedCount == that.skippedCount && totalCount == that.totalCount && rightCount == that.rightCount && wrongCount == that.wrongCount;
    }

    @Override
    public int hashCode() {
        int result;
        result = rightCount;
        result = 31 * result + wrongCount;
        result = 31 * result + skippedCount;
        result = 31 * result + totalCount;
        return result;
    }

}
