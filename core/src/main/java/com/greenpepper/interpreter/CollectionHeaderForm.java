/*
 * Copyright (c) 2006 Pyxis Technologies inc.
 *
 * This is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This software is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301 USA,
 * or see the FSF site: http://www.fsf.org.
 */

package com.greenpepper.interpreter;

import com.greenpepper.interpreter.column.*;
import com.greenpepper.reflect.Fixture;
import com.greenpepper.util.StringUtil;

import java.util.regex.Pattern;

/**
 * <p>HeaderForm class.</p>
 *
 * @author oaouattara
 * @version $Id: $Id
 */
public final class CollectionHeaderForm
{

    private static final String defaultedRegexp = "(?i)^(.+)\\(\\s*(null|true|false|not\\s+null)\\s*\\)\\s*$";

    private final String text;

    /**
     * <p>parse.</p>
     *
     * @param text a {@link String} object.
     * @return a {@link CollectionHeaderForm} object.
     */
    public static CollectionHeaderForm parse(String text)
    {
        return new CollectionHeaderForm( text );
    }

    private CollectionHeaderForm(String text)
    {
        this.text = text;
    }

    private String header()
    {
        return text.trim();
    }

    private boolean expectFalse() {
        return Pattern.matches("(?i).+\\(\\s*false\\s*\\)\\s*$", header());
    }

    private boolean expectTrue() {
        return Pattern.matches("(?i).+\\(\\s*true\\s*\\)\\s*$", header());
    }

    private boolean expectNull() {
        return Pattern.matches("(?i).+\\(\\s*null\\s*\\)\\s*$", header());
    }

    private boolean expectNotNull() {
        return Pattern.matches("(?i).+\\(\\s*not\\s*null\\s*\\)\\s*$", header());
    }

    /**
     * <p>message.</p>
     *
     * @return a {@link String} object.
     */
    public String message()
    {
        return header().replaceAll(defaultedRegexp, "$1" );
    }

    /**
     * <p>selectColumn.</p>
     *
     * @return a {@link Column} object.
     * @throws Exception if any.
     */
    public ExpectedColumn selectColumn() throws Exception
    {
        if (isNull()) return new NullColumn();

        if (expectFalse()) return new ExpectFalseColumn(message());
        if (expectTrue()) return new ExpectTrueColumn(message());
        if (expectNull()) return new ExpectNullColumn(message());
        if (expectNotNull()) return new ExpectNotNullColumn(message());

        return new ExpectedColumn(message());
    }

    /**
     * <p>isNull.</p>
     *
     * @return a boolean.
     */
    public boolean isNull()
    {
        return StringUtil.isBlank( text );
    }

    public static boolean isDefaulted(String header) {
        return Pattern.matches(defaultedRegexp, header);
    }

    public static String message(String header)
    {
        return header.replaceAll(defaultedRegexp, "$1" );
    }
}
