
/**
 * <p>ExpectedColumn class.</p>
 *
 * @author oaouattara
 * @version $Id: $Id
 */
package com.greenpepper.interpreter.column;

import com.greenpepper.Call;
import com.greenpepper.Example;
import com.greenpepper.Statistics;
import com.greenpepper.call.Annotate;
import com.greenpepper.call.Compile;
import com.greenpepper.expectation.ShouldBe;
import com.greenpepper.reflect.Message;
import com.greenpepper.util.StringUtil;

public class ExpectNullColumn extends ExpectedColumn
{

    public ExpectNullColumn(String header) {
        super(header);
    }

    @Override
    protected void setCallExpectation(Example cell, Call call) {
        call.expect( ShouldBe.NULL );
    }
}
