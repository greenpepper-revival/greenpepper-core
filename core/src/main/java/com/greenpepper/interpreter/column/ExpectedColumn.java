
/**
 * <p>ExpectedColumn class.</p>
 *
 * @author oaouattara
 * @version $Id: $Id
 */
package com.greenpepper.interpreter.column;

import com.greenpepper.Call;
import com.greenpepper.Example;
import com.greenpepper.Statistics;
import com.greenpepper.call.Annotate;
import com.greenpepper.call.Compile;
import com.greenpepper.expectation.ShouldBe;
import com.greenpepper.reflect.Message;
import com.greenpepper.util.StringUtil;
public class ExpectedColumn extends Column
{
    private Message check;
    private String header;

    public ExpectedColumn(String header) {
        this.header = header;
    }

    /**
     * <p>Constructor for ExpectedColumn.</p>
     *
     * @param check a {@link com.greenpepper.reflect.Message} object.
     */
    public ExpectedColumn( Message check ) {
        this.check = check;
    }

    /** {@inheritDoc} */
    public Statistics doCell( Example cell ) throws Exception
    {
        return doCell(cell, new Statistics(), true);
    }

    public Statistics doCell(Example cell, boolean annotateCell) throws Exception {
        return doCell(cell, new Statistics(), annotateCell);
    }

    public Statistics doCell(Example cell, Statistics stats) throws Exception {
        return doCell(cell, stats, true);
    }

    public Statistics doCell(Example cell, Statistics stats, boolean annotateCell) throws Exception {
        Call call = new Call( check );
        setCallExpectation(cell, call);
        if (annotateCell) call.will( Annotate.withDetails( cell ) );
        call.will( Compile.statistics( stats ) );
        call.execute();
        return stats;
    }

    protected void setCallExpectation(Example cell, Call call) {
        if (!StringUtil.isBlank( cell.getContent() ))
        {
            call.expect( ShouldBe.literal( cell.getContent() ) );
        }
    }

    public void setCheck(Message check) {
        this.check = check;
    }

    public String header() {
        return header;
    }
}
