package com.greenpepper.interpreter.flow.dowith;

import com.greenpepper.GreenPepper;
import com.greenpepper.annotation.Fixture;
import com.greenpepper.annotation.FixtureMethod;
import com.greenpepper.interpreter.MockFixture;
import com.greenpepper.interpreter.flow.Row;
import com.greenpepper.reflect.Type;

import java.util.Locale;

@Fixture(usage = "Dowith action language support")
public class DowithActionLanguageSupportFixture {

	public String interpreterName;
	public String locale;

	@FixtureMethod(usage = "interpreter class ")
	public String interpreterClass() {
		GreenPepper.setLocale(new Locale(locale));
		Type<Row> rowType = new DoWithRowSelector(new MockFixture()).getTypeLoader().loadType(interpreterName);
		if (rowType != null) return rowType.getUnderlyingClass().getSimpleName();
		return null;
	}
}