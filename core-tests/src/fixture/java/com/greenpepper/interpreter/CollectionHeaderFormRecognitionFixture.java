package com.greenpepper.interpreter;

import com.greenpepper.interpreter.column.*;

public class CollectionHeaderFormRecognitionFixture
{
    private String headerText;

    public String  columnDefaultValue() throws Exception
    {

        Column col = CollectionHeaderForm.parse(headerText).selectColumn();
        if (col instanceof ExpectFalseColumn) return "false";
        else if (col instanceof NullColumn) return "ignore";
        else if (col instanceof ExpectTrueColumn) return "true";
        else if (col instanceof ExpectNullColumn) return null;
        else if (col instanceof ExpectNotNullColumn) return "not null";
        else return "expected";
    }

    public void setHeaderText(String text)
    {
        if (text != null)
        {
            headerText = text;
        }
        else
        {
            headerText = "";
        }
    }
 }
